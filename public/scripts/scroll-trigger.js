function scrollTrigger(selector) {
    let els = document.querySelectorAll(selector);
    els = Array.from(els);
    els.forEach((el) => {
        addObserver(el);
    });
}
function addObserver(el) {
    let observer = new IntersectionObserver((entries, observer) => {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {
                entry.target.classList.add("visible-on-scroll");
            } else {
                entry.target.classList.remove("visible-on-scroll");
            }
        });
    });
    observer.observe(el);
}
scrollTrigger(".adopt-header-container");
scrollTrigger(".aboutus-header-container");
scrollTrigger(".adopt-part-1");
scrollTrigger(".adopt-part-2");
scrollTrigger(".adopt-part-3");
scrollTrigger(".donate-container");
scrollTrigger(".aboutus-body");
