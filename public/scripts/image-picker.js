const images = document.getElementsByClassName("aboutus-img");

images[images.length - 1].classList.add("main-img");

function setMain(id) {
    const mainImage = document.querySelector("#" + id);
    if (mainImage && !mainImage.classList.contains("main-img")) {
        mainImage.classList.add("main-img");
        for (let index = 0; index < images.length; index++) {
            const i = images[index];
            if (i.id !== id) {
                i.classList.remove("main-img");
                i.classList.add("not-main-img-" + index + 1);
            }
        }
    }
}

function setNotMain(id) {
    const mainImage = document.querySelector("#" + id);
    if (mainImage && mainImage.classList.contains("main-img")) {
        mainImage.classList.remove("main-img");

        for (let index = 0; index < images.length; index++) {
            const i = images[index];
            if (i.id === id) {
                mainImage.classList.add("not-main-img-" + index + 1);
            }
            if (index === images.length - 1) {
                i.classList.add("main-img");
            }
        }
    }
}
